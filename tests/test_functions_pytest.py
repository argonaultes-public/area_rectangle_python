import sys
import pytest

sys.path.append('../')
from src.functions  import calculate_area_rectangle


def test_simple():
    assert calculate_area_rectangle(2, 3) == 6

def test_float():
    assert calculate_area_rectangle(2.3, 1.0) == 2.3

@pytest.mark.parametrize("length,long", [(-1, 8), (2, -6), (-1, -42)])
def test_negative(length, long):
    with pytest.raises(ValueError):
        calculate_area_rectangle(length, long)

@pytest.mark.parametrize("length,long", [("-1", 8), (2, "-6"), ("-1", "-42"), (True, 1), (1, True), (False, True), (None, 10)])
def test_wrong_type(length, long):
    with pytest.raises(TypeError):
        calculate_area_rectangle(length, long)
