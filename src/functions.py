def calculate_area_rectangle(length, long):
    if is_not_int_nor_float(length) or is_not_int_nor_float(long):
        raise TypeError('Unexpected type')
    if length < 0 or long < 0:
        raise ValueError('Unexpected negative value')
    return length * long

def is_not_int_nor_float(value):
    return type(value) is not int and type(value) is not float